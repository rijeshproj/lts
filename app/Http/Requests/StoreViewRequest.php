<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreViewRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value' => 'required|max:50|alpha_num'
        ];
    }

    public function messages()
    {
        return [
            'value.required' => 'A view is required',
            'value.alpha_num'  => 'View should be alpha numeric',
            'value.max' => "Maximum length should be less than 50"
        ];
    }
}
