<?php

namespace App\Http\Controllers;

use App\MSValue;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreViewRequest;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function store(StoreViewRequest $request)
    {
        $MSValue = new MSValue;

        if ($MSValue->insert($request))
        {
            $request->session()->flash('alert-success', 'Value was successful added!');
        }
        else
        {
            $request->session()->flash('alert-danger', 'Something went wrong!');
        }

        return redirect('home');
    }


}
