<?php

namespace App\Listeners;

use App\Events\LoginEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


class LoginEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  SomeEvent  $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {
        $data['name'] = $event->user->name;

        Mail::send('email.login', $data, function ($message) {
            $message->from('rijeshpv84test@gmail.com', 'My App');

            $message->to(env('LOGIN_TO_EMAIL'));
        });

    }
}
