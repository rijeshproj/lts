@include('includes.header')

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->

        <div class="row">
            <div class="col-12">

                <form class="form-horizontal" role="form" method="POST" action="{{ url('auth/login') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="row">
                        <div class="col-4 col-offset-4" style="margin: 0 auto;padding-top: 20%">
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                </div>
                                <div class="panel-body">


                                    <div class="omb_login">

                                        <div class="row omb_row-sm-offset-3 omb_socialButtons">
                                            <div class="col-xs-4 col-sm-2">
                                                <a href="{{ url('auth/facebook') }}" class="btn btn-lg btn-block omb_btn-facebook" style="width: 300px">
                                                    <i class="fa fa-facebook visible-xs"></i>
                                                    <span class="hidden-xs">Facebook Login</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>


@include('includes.footer')

