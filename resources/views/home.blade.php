@include('includes.header')

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->

        <div class="row">
            <div class="col-12">

                <a href="{{url('auth/logout')}}" class="pull-right">Logout</a>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('value-store') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="row">
                        <div class="col-4 col-offset-4" style="margin: 0 auto;padding-top: 20%">
                            <div class="panel panel-default">

                                <div class="panel-body">


                                        @if (count($errors) > 0)
                                            <div class="alert alert-danger">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        @endif

                                        <div class="flash-message">
                                            @if(Session::has('alert-success'))
                                                <p class="alert alert-success">{{ Session::get('alert-success') }}</p>
                                            @endif
                                            @if(Session::has('alert-danger'))
                                                <p class="alert alert-danger">{{ Session::get('alert-danger') }}</p>
                                            @endif
                                        </div>

                                        <fieldset>
                                            <div class="form-group">
                                                Value : <input class="form-control" placeholder="Value" name="value" type="text" autofocus>
                                            </div>

                                            <!-- Change this to a button or input when using this as a form -->
                                            <button type="submit" class="btn btn-success btn-block">Save</button>

                                        </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>
</div>


@include('includes.footer')

